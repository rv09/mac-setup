#!/bin/sh
echo
echo "> Setting up shell"

finger raviv | grep -q "Shell: /bin/zsh"
if [ $? != 0 ]; then
  echo ">> Changing default shell to zsh"
  chsh -s /bin/zsh
fi

echo ">> Install OMZ"
sh ./shell/omz/install.sh

echo ">> Setting up aliases"
sh ./shell/alias/setup.sh

sh ./shell/git/config.sh

# hide last login line from the terminal
touch ~/.hushlogin