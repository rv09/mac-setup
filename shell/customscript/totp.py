#!/usr/bin/env python3
import pyotp
def tfa_now(secret="your_secret_key"):
    totp = pyotp.TOTP(secret)
    return totp.now()

#print(tfa_now())