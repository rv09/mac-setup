#!/bin/sh

sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

cp ~/.zshrc ~/.zshrc_backup

# theme
git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
theme="powerlevel10k/powerlevel10k"
cp ~/.zshrc ~/.zshrc.lock \
&& sed "s|ZSH_THEME=".*"|ZSH_THEME=\"$theme\"|g" ~/.zshrc.lock > ~/.zshrc \
&& rm ~/.zshrc.lock

# plugin
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/blimmer/zsh-aws-vault.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-aws-vault
plugins="git zsh-syntax-highlighting zsh-autosuggestions zsh-aws-vault"
cp ~/.zshrc ~/.zshrc.lock \
&& sed "s|plugins=(.*)|plugins=( $plugins )|g" ~/.zshrc.lock > ~/.zshrc \
&& rm ~/.zshrc.lock
