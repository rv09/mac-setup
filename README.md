# Setting up your machine

1. Ensure your machine is running (at least) OSX Catalina
1. Run `bash <(curl -s https://gitlab.com/rv09/mac-setup/-/raw/main/bootstrap.sh)`
1. Login to the App Store in the background (before mac apps get installed)
1. Restore the list of Manual Items (below)
1. Log out and back in to apply OSX changes


# Manual items

## Karabiner
Open `Karabiner > Complex Modifications > Add rule > Enable All under *Motospeed CK62 Layer Fix*`

## Alfred
Setup theme

## iTerm2
Open `Preferences > General > Preferences` and set the config path to `<base-path>/mac-setup/iterm2`

Make changes via the iterm2 UI and commit the file.

Run `p10k configure` to configure theme if it doesn't ask on first launch

## ssh

Run `ssh-keygen -t ed25519 -C "<comment>"` to generate ssh key 

Run to copy generated public key `tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy`

# TODO

- Arc in Brewfile (waiting for Arc to be in brew)
- Arc as default browser
- [FIX] mysides to set Projects and user dir in finder favourites
- Setup for rectangle shortcuts
- Check if brew install for karbiner, powershell & zoom can be done without entering passwords