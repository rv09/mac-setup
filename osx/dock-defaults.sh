#!/bin/sh

source common/functions.sh

delete_if_available com.apple.dock persistent-apps

__dock_item() {
  printf '%s%s%s%s%s' \
         '<dict><key>tile-data</key><dict><key>file-data</key><dict>' \
         '<key>_CFURLString</key><string>' \
         "$1" \
         '</string><key>_CFURLStringType</key><integer>0</integer>' \
         '</dict></dict></dict>'
}


# Declare all apps here, can be multiple

launchpad=$(__dock_item /System/Applications/Launchpad.app)

defaults write com.apple.dock persistent-apps -array "$launchpad"

killall Dock
